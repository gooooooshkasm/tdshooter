// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooterGameMode.h"
#include "TDShooterPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "TDShooterCharacter.h"

ATDShooterGameMode::ATDShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprint/Game/BP_PlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}
